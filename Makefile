
all:
	make -j32 -C libft
	make -j32 -C printf
	make -j32 -C client_project client_only
	make -j32 -C server_project server_only
	cp client_project/client client
	cp server_project/server server

client:
	make -j32 -C libft
	make -j32 -C printf
	make -j32 -C client_project
	cp client_project/client client

server:
	make -j32 -C libft
	make -j32 -C printf
	make -j32 -C server_project
	cp server_project/server server

clean:
	make -C client_project clean
	make -C server_project self_clean

fclean:
	make -C client_project fclean
	make -C server_project self_fclean
	rm -f server client

re:
	make -C client_project fclean
	make -C server_project self_fclean
	make -j32 -C libft
	make -j32 -C printf
	make -j32 -C client_project client_only
	make -j32 -C server_project server_only
	cp client_project/client client
	cp server_project/server server
