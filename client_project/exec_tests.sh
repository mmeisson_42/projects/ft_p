#! /bin/bash

if [ -z "$1" ]
then
	echo "Usage: $0 timeToSleep" >&2
	exit 1
fi

for test in $(ls test_set)
do
	echo -e '\033[032m'
	cat test_set"/$test"
	echo -e '\033[0m'
	sleep 2
	./client 127.0.0.1 3334 < test_set"/$test"
	sleep $1
	clear
done
