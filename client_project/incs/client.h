/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:23:40 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:24:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_H
# define CLIENT_H

# define PROMPT "\n$> "

struct				s_context
{
	char	*host;

	char	*username;

	int		fd_command;
	int		fd_data;
	int		data_mode;
};

typedef void		(*t_command_callback)(
	const char **command,
	struct s_context *);

struct				s_commands
{
	char				name[8];
	t_command_callback	callback;
};

void				client_loop(struct s_context *ctx);
struct s_channel	passiv_stream(struct s_context *ctx);
struct s_channel	activ_stream(struct s_context *ctx);
void				local_exec(const char *path, char **args);

void				commands(
	const char **command,
	struct s_context *ctx);
char				*send_command(
	const char *command,
	struct s_context *ctx);
void				fatal_error(
	const char *explanation,
	struct s_context *ctx);
char				*generate_command(
	const char *command_name,
	const char **options,
	struct s_context *ctx);
int					get_stream(
	struct s_channel *activ_channel,
	struct s_context *ctx,
	const char *command_to_send);

/*
**	Commands
*/
void				ls(const char **command, struct s_context *ctx);
void				login(const char **command, struct s_context *ctx);
void				pwd(const char **command, struct s_context *ctx);
void				quit(const char **command, struct s_context *ctx);
void				get(const char **command, struct s_context *ctx);
void				put(const char **command, struct s_context *ctx);
void				cd(const char **command, struct s_context *ctx);
void				cdup(const char **command, struct s_context *ctx);
void				mkdir(const char **command, struct s_context *ctx);
void				rmd(const char **command, struct s_context *ctx);
void				rm(const char **command, struct s_context *ctx);
void				lls(const char **command, struct s_context *ctx);
void				lpwd(const char **command, struct s_context *ctx);
void				lcd(const char **command, struct s_context *ctx);
void				bin(const char **command, struct s_context *ctx);
void				ascii(const char **command, struct s_context *ctx);

#endif
