/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   activ_stream.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:35:48 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:37:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "libft.h"
#include "client.h"
#include "common.h"

static char					*get_ipv4(
	char *base_address,
	struct s_channel c)
{
	char	*address_compounds[4];
	char	*address;

	base_address = ft_strdup(base_address);
	if (base_address == NULL)
		return (NULL);
	address_compounds[3] = NULL;
	if (
		(address_compounds[0] = ft_strreplace(base_address, '.', ',')) == NULL
		|| (address_compounds[1] = ft_itoa((int)((c.port >> 8) & 0xff))) == NULL
		|| (address_compounds[2] = ft_itoa((int)(c.port & 0xff))) == NULL)
	{
		ft_tabiter(address_compounds, free);
		return (NULL);
	}
	address = ft_tabjoin((const char **)address_compounds, ',');
	ft_tabiter(address_compounds, free);
	return (address);
}

static struct s_channel		generate_port_command(
	struct s_context *ctx,
	char **command)
{
	struct s_channel		c;
	char					*address;

	c = bind_any_port();
	if (c.fd != -1)
	{
		address = get_ipv4(ctx->host, c);
		if (address == NULL)
			return (c);
		*command = ft_strjoin(PORT_COMMAND" ", address);
		ft_strdel(&address);
		if (*command == NULL)
			return (c);
	}
	else
	{
		ft_putstr(" /!\\ Could not bind to any port\n");
	}
	return (c);
}

struct s_channel			activ_stream(struct s_context *ctx)
{
	char				*command;
	char				*response;
	struct s_channel	c;

	command = NULL;
	c = generate_port_command(ctx, &command);
	if (command == NULL)
	{
		c.fd = -1;
		return (c);
	}
	response = send_command(command, ctx);
	ft_strdel(&command);
	if (response == NULL || response[0] != '2')
	{
		close(c.fd);
		c.fd = -1;
	}
	ft_strdel(&response);
	return (c);
}
