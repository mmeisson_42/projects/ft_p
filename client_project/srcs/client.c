/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:33:30 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:33:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <signal.h>
#include "client.h"
#include "common.h"
#include "libft.h"
#include "ft_stdio.h"

static void		sig_handler(void *param)
{
	static struct s_context		*c = NULL;

	if (c == NULL)
	{
		c = param;
		return ;
	}
	ft_dprintf(
		2,
		"Signal received (%lu)\n\tClosing connections...\n",
		(size_t)param);
	if (c->fd_command != -1)
	{
		close(c->fd_command);
	}
	if (c->fd_data != -1)
	{
		close(c->fd_data);
	}
	exit(1);
}

static void		sig(void)
{
	int		i;

	i = 0;
	while (i < SIGPIPE)
	{
		signal(i, (void (*)(int))sig_handler);
		i++;
	}
}

int				main(int argc, const char **argv)
{
	struct s_context	ctx;
	unsigned short		port;

	if (argc != 3)
	{
		ft_dprintf(2, "Usage: %s host port\n", argv[0]);
		return (EXIT_FAILURE);
	}
	sig_handler(&ctx);
	sig();
	ft_bzero(&ctx, sizeof(ctx));
	ctx.host = resolve_host((char *)argv[1]);
	ctx.data_mode = MODE_ASCII;
	if (ctx.host == NULL)
		fatal_error(ERR_OOM, &ctx);
	port = ft_atoi(argv[2]);
	ctx.fd_command = socket_listener(ctx.host, port);
	if (ctx.fd_command == -1)
	{
		ft_putstr_fd("Could not connect to this server :(\n", 2);
		return (EXIT_FAILURE);
	}
	client_loop(&ctx);
	close(ctx.fd_command);
	return (EXIT_SUCCESS);
}
