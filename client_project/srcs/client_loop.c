/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_loop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:35:11 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:35:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "client.h"
#include "libft.h"
#include "ft_stdio.h"
#include "get_next_line.h"

void		client_loop(struct s_context *ctx)
{
	char				*str;
	char				**command;

	str = NULL;
	get_next_line(ctx->fd_command, &str);
	ft_printf(str);
	ft_strdel(&str);
	ft_putstr(PROMPT);
	while (get_next_line(STDIN_FILENO, &str) > 0)
	{
		command = ft_strsplit(str, ' ');
		if (command != NULL && command[0])
		{
			commands((const char **)command, ctx);
		}
		ft_tabdel(&command);
		ft_strdel(&str);
		ft_putstr(PROMPT);
	}
	quit(NULL, ctx);
}
