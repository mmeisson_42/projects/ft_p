/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:24:42 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:25:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "client.h"
#include "libft.h"

static const struct s_commands		client_commands[] = {
	{.name = "ls", .callback = ls},
	{.name = "login", .callback = login},
	{.name = "pwd", .callback = pwd},
	{.name = "quit", .callback = quit},
	{.name = "get", .callback = get},
	{.name = "put", .callback = put},
	{.name = "cd", .callback = cd},
	{.name = "cdup", .callback = cdup},
	{.name = "mkdir", .callback = mkdir},
	{.name = "rmd", .callback = rmd},
	{.name = "rm", .callback = rm},
	{.name = "lls", .callback = lls},
	{.name = "lpwd", .callback = lpwd},
	{.name = "lcd", .callback = lcd},
	{.name = "bin", .callback = bin},
	{.name = "ascii", .callback = ascii},
};

void	commands(const char **command, struct s_context *ctx)
{
	size_t		i;

	i = 0;
	while (i < (sizeof(client_commands) / sizeof(client_commands[0])))
	{
		if (ft_strcmp(command[0], client_commands[i].name) == 0)
		{
			client_commands[i].callback(command, ctx);
			return ;
		}
		i++;
	}
	ft_putstr("Error : This command does not exists :(\n");
}
