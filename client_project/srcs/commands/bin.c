/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bin.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:32:57 by mmeisson          #+#    #+#             */
/*   Updated: 2019/01/17 09:19:58 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"
#include "common.h"
#include "libft.h"
#include "ft_stdio.h"

void		bin(const char **command, struct s_context *ctx)
{
	char		*response;

	(void)command;
	response = send_command(TYPE_COMMAND" I", ctx);
	if (response[0] == '1' || response[0] == '2')
	{
		ctx->data_mode = MODE_BINARY;
		ft_putstr("Binary mode activated\n");
	}
	else
	{
		ft_putstr("Failed to activ binary mode\n");
	}
	ft_strdel(&response);
}
