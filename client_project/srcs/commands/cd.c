/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:32:57 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:32:59 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"
#include "common.h"
#include "libft.h"
#include "ft_stdio.h"

void		cd(const char **command, struct s_context *ctx)
{
	char	*to_send;
	char	*response;

	if (ft_tablen(command) != 2)
	{
		ft_dprintf(2, "Usage: %s directory\n", command[0]);
		return ;
	}
	to_send = ft_strjoin(CWD_COMMAND" ", command[1]);
	response = send_command(to_send, ctx);
	ft_strdel(&to_send);
	ft_strdel(&response);
}
