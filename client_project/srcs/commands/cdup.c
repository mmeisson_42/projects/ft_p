/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cdup.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:31:57 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:32:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"
#include "common.h"
#include "libft.h"

void		cdup(const char **command, struct s_context *ctx)
{
	char	*response;

	(void)command;
	response = send_command(CDUP_COMMAND, ctx);
	ft_strdel(&response);
}
