/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:31:46 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:31:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <fcntl.h>
#include <sys/socket.h>
#include "libft.h"
#include "ft_stdio.h"
#include "client.h"
#include "common.h"

static void		unixified_stream_data(int to_fd, int from_fd, int data_mode)
{
	char		buffer[4096];
	ssize_t		ret;
	char		*convert;

	if (to_fd != -1 && from_fd != -1)
	{
		while ((ret = read(from_fd, buffer, 4095)) > 0)
		{
			if (data_mode == MODE_BINARY)
			{
				write(from_fd, buffer, ret);
			}
			else
			{
				buffer[ret] = 0;
				convert = window_to_unix(buffer);
				if (convert != NULL)
				{
					write(to_fd, convert, ft_strlen(convert));
					free(convert);
				}
			}
		}
	}
}

static void		store_file(const char *file, int stream_fd, int data_mode)
{
	char		*file_name;
	int			file_fd;

	file_name = ft_strrchr(file, '/') + 1;
	if (file_name == NULL + 1)
		file_name = (char *)file;
	file_fd = open(file_name, O_WRONLY | O_CREAT, 0644);
	if (file_fd == -1)
	{
		ft_dprintf(2, "Could not open local file %s\n", file);
		return ;
	}
	unixified_stream_data(file_fd, stream_fd, data_mode);
	close(file_fd);
}

void			get(const char **command, struct s_context *ctx)
{
	struct s_channel	activ_channel;
	int					stream_fd;
	char				*str;

	if (ft_tablen(command) != 2)
	{
		ft_dprintf(2, "Usage: %s [file_name]\n", command[0]);
		return ;
	}
	activ_channel.fd = -1;
	str = ft_strjoin(RETR_COMMAND" ", command[1]);
	if (str != NULL)
	{
		stream_fd = get_stream(&activ_channel, ctx, str);
		if (stream_fd != -1)
		{
			store_file(command[1], stream_fd, ctx->data_mode);
			close(stream_fd);
		}
		free(str);
		str = send_command(NULL, ctx);
		ft_strdel(&str);
	}
	if (activ_channel.fd != -1)
		close(activ_channel.fd);
}
