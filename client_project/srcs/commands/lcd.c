/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lcd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 15:22:52 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/18 15:23:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "common.h"
#include "client.h"
#include "libft.h"
#include "ft_stdio.h"

char		*get_home(void)
{
	const char		home[] = "HOME";
	extern char		**environ;
	char			*res;

	res = NULL;
	while (*environ)
	{
		if (ft_strncmp(home, *environ, ft_strlen(home)) == 0)
		{
			res = ft_strchr(*environ, '=');
			if (res)
			{
				res += 1;
			}
			break ;
		}
		environ++;
	}
	return (res);
}

void		lcd(const char **command, struct s_context *ctx)
{
	char	*required_path;

	if (ft_tablen(command) > 2)
	{
		ft_printf("Usage: %s [path]", command[0]);
		return ;
	}
	if (command[1] != NULL)
	{
		required_path = (char *)command[1];
	}
	else
	{
		required_path = get_home();
	}
	if (required_path != NULL && chdir(required_path) == 0)
	{
		ft_putstr("New path:");
		lpwd(command, ctx);
	}
	else
	{
		ft_printf("Failed to access path %s\n", required_path);
	}
}
