/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lls.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 15:21:21 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/18 15:22:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "client.h"
#include "common.h"
#include <stdlib.h>

static char		**fill_new_command(char **filtered, const char **command)
{
	size_t		command_index;
	size_t		filtered_index;

	command_index = 1;
	filtered_index = 2;
	while (command[command_index] != NULL && command[command_index][0] == '-')
	{
		if (ft_strcmp(command[command_index++], "--") == 0)
			break ;
	}
	while (command[command_index] != NULL)
	{
		filtered[filtered_index] = ft_strdup(command[command_index++]);
		if (filtered[filtered_index++] == NULL)
		{
			ft_tabdel(&filtered);
			return (NULL);
		}
	}
	filtered[filtered_index] = NULL;
	return (filtered);
}

static char		**filter_options(const char **command)
{
	char		**filtered;

	filtered = malloc(sizeof(char *) * (ft_tablen(command) + 2));
	if (filtered == NULL)
	{
		return (NULL);
	}
	if ((filtered[0] = ft_strdup(command[0])) == NULL
			|| (filtered[1] = ft_strdup("-l")) == NULL)
	{
		ft_tabdel(&filtered);
	}
	return (fill_new_command(filtered, command));
}

void			lls(const char **command, struct s_context *ctx)
{
	const char	*path = "/bin/ls";
	char		**filtered;

	filtered = filter_options(command);
	if (filtered == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	local_exec(path, filtered);
	ft_tabdel(&filtered);
}
