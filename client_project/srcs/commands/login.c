/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   login.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:31:01 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:31:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"
#include "libft.h"
#include "common.h"
#include "ft_stdio.h"
#include "get_next_line.h"
#include <sys/ioctl.h>
#include <termios.h>

static char		*get_username(const char **command)
{
	char	*username;

	if (command[1] == NULL)
	{
		ft_putstr("\tusername: ");
		get_next_line(0, &username);
	}
	else
	{
		username = ft_strdup(command[1]);
	}
	return (username);
}

static char		*get_password(void)
{
	char			*password;
	struct termios	old_tty;
	struct termios	tty;

	password = NULL;
	ioctl(0, TCGETA, &old_tty);
	ft_putstr("\tpassword (hidden): ");
	tty = old_tty;
	tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL);
	tty.c_cc[VMIN] = 1;
	tty.c_cc[VTIME] = 0;
	ioctl(0, TCSETA, &tty);
	if (get_next_line(0, &password) < 1)
	{
		password = NULL;
	}
	ioctl(0, TCSETA, &old_tty);
	return (password);
}

static void		handle_password(struct s_context *ctx)
{
	char	*response;
	char	*password;
	char	*to_send;

	password = get_password();
	if (password == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	to_send = ft_strjoin(PASS_COMMAND" ", password);
	free(password);
	if (to_send == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	response = send_command(to_send, ctx);
	ft_strdel(&response);
	free(to_send);
}

void			login(const char **command, struct s_context *ctx)
{
	char	*to_send;
	char	*response;

	ft_strdel(&ctx->username);
	ctx->username = get_username(command);
	if (ctx->username == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	to_send = ft_strjoin(USER_COMMAND" ", ctx->username);
	if (to_send == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	response = send_command(to_send, ctx);
	if (response == NULL)
	{
		fatal_error(ERR_NOCONN, ctx);
	}
	if (ft_strncmp("331", response, 3) == 0)
	{
		handle_password(ctx);
	}
	ft_strdel(&to_send);
	ft_strdel(&response);
}
