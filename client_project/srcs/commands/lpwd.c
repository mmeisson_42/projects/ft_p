/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lpwd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:31:57 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:32:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "common.h"
#include "client.h"
#include "libft.h"

void		lpwd(const char **command, struct s_context *ctx)
{
	char	*cwd;

	(void)command;
	cwd = getcwd(NULL, 0);
	if (cwd != NULL)
	{
		ft_putendl(cwd);
		free(cwd);
	}
	else
	{
		fatal_error(ERR_OOM, ctx);
	}
}
