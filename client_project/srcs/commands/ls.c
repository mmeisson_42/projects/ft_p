/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 15:57:32 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 15:57:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "client.h"
#include "common.h"
#include "ft_stdio.h"
#include "get_next_line.h"

static void		print_ls_stream(int fd)
{
	char	*str;

	while (get_next_line(fd, &str) > 0)
	{
		ft_printf("%s\n", str);
		ft_strdel(&str);
	}
}

void			ls(const char **command, struct s_context *ctx)
{
	struct s_channel	activ_channel;
	int					stream_fd;
	char				*lst_cmd;
	char				*response;

	activ_channel.fd = -1;
	lst_cmd = generate_command(LIST_COMMAND, command + 1, ctx);
	if (lst_cmd != NULL)
	{
		stream_fd = get_stream(&activ_channel, ctx, lst_cmd);
		ft_strdel(&lst_cmd);
		if (stream_fd == -1)
			return ;
		print_ls_stream(stream_fd);
		close(stream_fd);
		response = send_command(NULL, ctx);
		ft_strdel(&response);
	}
	ft_strdel(&lst_cmd);
	if (activ_channel.fd != -1)
		close(activ_channel.fd);
}
