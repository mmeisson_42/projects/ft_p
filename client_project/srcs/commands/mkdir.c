/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mkdir.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 19:38:37 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 19:38:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "client.h"
#include "common.h"
#include "libft.h"

void	mkdir(const char **command, struct s_context *ctx)
{
	char		*mkdir_command;
	char		*response;

	mkdir_command = generate_command(MKD_COMMAND, command + 1, ctx);
	if (mkdir_command == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	response = send_command(mkdir_command, ctx);
	ft_strdel(&response);
	free(mkdir_command);
}
