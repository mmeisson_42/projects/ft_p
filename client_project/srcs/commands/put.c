/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:27:23 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:29:10 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <fcntl.h>
#include <sys/socket.h>
#include "libft.h"
#include "ft_stdio.h"
#include "client.h"
#include "common.h"

static int		get_file(const char *file)
{
	char		*file_name;
	int			file_fd;

	file_name = ft_strrchr(file, '/');
	if (file_name == NULL)
		file_name = (char *)file;
	file_fd = open(file_name, O_RDONLY);
	if (file_fd == -1)
	{
		ft_dprintf(2, "Could not open local file %s\n", file);
	}
	return (file_fd);
}

static void		put_file(
	const char *put_cmd,
	int file_fd,
	struct s_context *ctx)
{
	struct s_channel	activ_channel;
	int					stream_fd;
	char				*response;

	activ_channel.fd = -1;
	stream_fd = get_stream(&activ_channel, ctx, put_cmd);
	if (stream_fd != -1)
	{
		stream_data(stream_fd, file_fd, MODE_BINARY);
		close(stream_fd);
	}
	close(file_fd);
	response = send_command(NULL, ctx);
	ft_strdel(&response);
	if (activ_channel.fd != -1)
	{
		close(activ_channel.fd);
	}
}

void			put(const char **command, struct s_context *ctx)
{
	char				*put_cmd;
	int					file_fd;

	if (ft_tablen(command) != 2)
	{
		ft_dprintf(2, "Usage: %s [file_name]\n", command[0]);
		return ;
	}
	file_fd = get_file(command[1]);
	if (file_fd == -1)
	{
		return ;
	}
	put_cmd = ft_strjoin(STOR_COMMAND" ", command[1]);
	if (put_cmd != NULL)
	{
		put_file(put_cmd, file_fd, ctx);
		free(put_cmd);
	}
}
