/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:30:34 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:30:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"
#include "common.h"
#include "libft.h"

void		pwd(const char **command, struct s_context *ctx)
{
	char	*response;

	(void)command;
	response = send_command(PWD_COMMAND, ctx);
	ft_strdel(&response);
}
