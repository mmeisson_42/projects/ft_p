/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:30:54 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:30:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "client.h"
#include "common.h"

void		quit(const char **command, struct s_context *ctx)
{
	char	*response;

	(void)command;
	ft_putstr_fd("Exiting this session...\n", 2);
	response = send_command(QUIT_COMMAND, ctx);
	if (ctx->fd_command != -1)
		close(ctx->fd_command);
	if (ctx->fd_data != -1)
		close(ctx->fd_data);
	ft_strdel(&response);
	exit(0);
}
