/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fatal_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:29:57 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:30:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include "client.h"

void	fatal_error(const char *explanation, struct s_context *ctx)
{
	if (explanation != NULL)
	{
		ft_putendl_fd(explanation, 2);
	}
	quit(NULL, ctx);
}
