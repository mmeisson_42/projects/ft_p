/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate_command.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:29:39 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:29:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "common.h"
#include "client.h"

char		*generate_command(
	const char *command_name,
	const char **options,
	struct s_context *ctx)
{
	char	*joined_options;
	char	*command;

	joined_options = ft_tabjoin(options, ' ');
	if (joined_options == NULL)
	{
		return (NULL);
	}
	command_name = ft_strjoin(command_name, " ");
	if (command_name == NULL)
	{
		fatal_error(ERR_OOM, ctx);
	}
	command = ft_strjoin(command_name, joined_options);
	free((char *)command_name);
	free(joined_options);
	return (command);
}
