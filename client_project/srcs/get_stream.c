/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_stream.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:31:32 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:31:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include "libft.h"
#include "client.h"
#include "common.h"

static void		handle_error_response(
	int *stream_fd,
	int activ,
	struct s_channel *activ_channel)
{
	close(*stream_fd);
	if (activ)
	{
		close(activ_channel->fd);
		activ_channel->fd = -1;
	}
	*stream_fd = -1;
}

int				get_stream(
	struct s_channel *activ_channel,
	struct s_context *ctx,
	const char *command_to_send)
{
	int		stream_fd;
	char	*response;
	int		activ;

	activ = 0;
	stream_fd = passiv_stream(ctx).fd;
	if (stream_fd == -1)
	{
		*activ_channel = activ_stream(ctx);
		if (activ_channel->fd == -1)
			return (-1);
		activ = 1;
		response = send_command(command_to_send, ctx);
		stream_fd = accept(activ_channel->fd, NULL, 0);
	}
	else
		response = send_command(command_to_send, ctx);
	if (response == NULL || (response[0] != '2' && response[0] != '1'))
	{
		handle_error_response(&stream_fd, activ, activ_channel);
	}
	ft_strdel(&response);
	return (stream_fd);
}
