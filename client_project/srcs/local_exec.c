/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   local_exec.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 15:21:46 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/18 15:21:57 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

void	local_exec(const char *path, char **args)
{
	pid_t			pid;
	extern char		**environ;

	pid = fork();
	if (pid == 0)
	{
		execve(path, args, environ);
	}
	else
	{
		wait4(pid, NULL, 0, NULL);
	}
}
