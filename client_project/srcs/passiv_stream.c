/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   passiv_stream.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:29:17 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:29:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "client.h"
#include "common.h"

struct s_channel		passiv_stream(struct s_context *ctx)
{
	char				*response;
	char				**network;
	struct s_channel	c;

	c.fd = -1;
	c.port = 0;
	response = send_command(PASV_COMMAND, ctx);
	if (response != NULL)
	{
		if (response[0] == '1' || response[0] == '2')
		{
			network = get_network(response);
			if (network != NULL)
			{
				c.port = ft_atoi(network[1]);
				c.fd = socket_listener(ctx->host, c.port);
			}
			ft_tabdel(&network);
		}
		ft_strdel(&response);
	}
	return (c);
}
