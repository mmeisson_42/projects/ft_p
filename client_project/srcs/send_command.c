/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   send_command.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 13:26:05 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 13:26:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <sys/time.h>
#include "client.h"
#include "libft.h"
#include "get_next_line.h"
#include "ft_stdio.h"

char			*send_command(
	const char *command,
	struct s_context *ctx)
{
	char	*response;

	response = NULL;
	if (command != NULL)
	{
		ft_putendl_fd(command, ctx->fd_command);
		ft_putendl(command);
	}
	ft_putstr("\033[93mWaiting for answer...\033[0m");
	if (get_next_line(ctx->fd_command, &response) > 0)
	{
		ft_putstr("\033[2K\r");
		if (response && (response[0] == '1' || response[0] == '2'))
			ft_printf("\033[92m%s\033[0m\n", response);
		else if (response && response[0] == '3')
			ft_printf("\033[93m%s\033[0m\n", response);
		else if (response)
			ft_printf("\033[91m%s\033[0m\n", response);
	}
	return (response);
}
