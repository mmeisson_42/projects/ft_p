/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:34:55 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:35:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

# define PASV_COMMAND	"PASV"
# define LIST_COMMAND	"LIST"
# define PORT_COMMAND	"PORT"
# define USER_COMMAND	"USER"
# define CWD_COMMAND	"CWD"
# define PASS_COMMAND	"PASS"
# define QUIT_COMMAND	"QUIT"
# define PWD_COMMAND	"PWD"
# define RETR_COMMAND	"RETR"
# define STOR_COMMAND	"STOR"
# define CDUP_COMMAND	"CDUP"
# define SYST_COMMAND	"SYST"
# define NOOP_COMMAND	"NOOP"
# define DELE_COMMAND	"DELE"
# define RMD_COMMAND	"RMD"
# define MKD_COMMAND	"MKD"
# define TYPE_COMMAND	"TYPE"

# define ERR_OOM		"Out Of Memory"
# define ERR_NOCONN		"No connection available"

# define MODE_BINARY	0
# define MODE_ASCII		1

# ifdef __APPLE__
#  define TCGETA TIOCGETA
#  define TCSETA TIOCSETA
# endif

# define TIMEOUT 20

struct				s_channel
{
	unsigned short		port;
	int					fd;
};

int					socket_binded(int port);
int					socket_binded_v6(int port);
int					socket_listener(const char *addr, int port);

struct s_channel	bind_any_port(void);
int					stream_data(int to_fd, int from_fd, int data_mode);

char				**get_network(const char *coma_network);
char				*resolve_host(const char *host);

char				*window_to_unix(const char *str);
char				*unix_to_window(const char *str);

#endif
