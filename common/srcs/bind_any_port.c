/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bind_any_port.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:34:44 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:37:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "common.h"

struct s_channel	bind_any_port(void)
{
	struct s_channel	c;

	c.port = 1024;
	while (c.port < USHRT_MAX)
	{
		c.fd = socket_binded(c.port);
		if (c.fd != -1)
		{
			return (c);
		}
		c.port++;
	}
	c.port = 0;
	c.fd = -1;
	return (c);
}
