/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_network.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:33:40 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:33:41 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		**get_base_components(const char *comma_network)
{
	char	**splitted_comma;

	while (!ft_isdigit(*comma_network))
	{
		comma_network++;
	}
	splitted_comma = ft_strsplit(comma_network, ',');
	if (ft_tablen((const char **)splitted_comma) != 6)
	{
		ft_tabdel(&splitted_comma);
	}
	return (splitted_comma);
}

static char		*join_network(char **splitted_comma)
{
	char	*tmp;
	char	*network;

	tmp = splitted_comma[4];
	splitted_comma[4] = NULL;
	network = ft_tabjoin((const char **)splitted_comma, '.');
	splitted_comma[4] = tmp;
	return (network);
}

static char		**fill_network(char **splitted_comma, char **network)
{
	int		port;

	port = (ft_atoi(splitted_comma[4]) << 8) | ft_atoi(splitted_comma[5]);
	network[1] = ft_itoa(port);
	if (network[1] == NULL)
	{
		ft_tabdel(&splitted_comma);
		free(network);
		return (NULL);
	}
	network[0] = join_network(splitted_comma);
	ft_tabdel(&splitted_comma);
	if (network[0] == NULL)
	{
		free(network[1]);
		free(network);
		return (NULL);
	}
	return (network);
}

char			**get_network(const char *comma_network)
{
	char	**splitted_comma;
	char	**network;

	splitted_comma = get_base_components(comma_network);
	if (splitted_comma == NULL)
		return (NULL);
	network = ft_memalloc(sizeof(char *) * 3);
	if (network == NULL)
		return (NULL);
	return (fill_network(splitted_comma, network));
}
