/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_host.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 18:26:13 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/18 18:26:14 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "libft.h"

char		*resolve_host(const char *addr)
{
	struct hostent	*h;
	struct in_addr	**ai;
	char			*resolved;
	size_t			i;

	h = gethostbyname2(addr, AF_INET6);
	if (h == NULL)
		h = gethostbyname2(addr, AF_INET);
	if (h != NULL)
	{
		ai = (struct in_addr **)h->h_addr_list;
		i = 0;
		while (ai[i] != NULL)
		{
			resolved = inet_ntoa(*(ai[0]));
			if (resolved != NULL)
			{
				return (ft_strdup(resolved));
			}
			i++;
		}
	}
	return (NULL);
}
