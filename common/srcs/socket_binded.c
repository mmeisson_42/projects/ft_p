/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socket_binded.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:34:29 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:37:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>

int		socket_binded(unsigned short port)
{
	int					socket_fd;
	struct protoent		*protoent;
	struct sockaddr_in6	sockaddr;

	protoent = getprotobyname("tcp");
	if (protoent == NULL)
		return (-1);
	socket_fd = socket(AF_INET6, SOCK_STREAM, protoent->p_proto);
	if (socket_fd != -1)
	{
		sockaddr.sin6_family = AF_INET6;
		sockaddr.sin6_port = htons(port);
		sockaddr.sin6_addr = in6addr_any;
		if (bind(
				socket_fd,
				(struct sockaddr *)&sockaddr,
				sizeof(sockaddr)) != 0)
		{
			return (-1);
		}
		listen(socket_fd, 42);
	}
	return (socket_fd);
}
