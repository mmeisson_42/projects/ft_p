/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socket_listener.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:33:26 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/18 15:26:56 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include "libft.h"
#include "common.h"

static int		connect_to(int socket_fd, const char *addr, int port)
{
	struct sockaddr_in	sockaddr;

	ft_bzero(&sockaddr, sizeof(sockaddr));
	sockaddr.sin_family = AF_INET;
	sockaddr.sin_port = htons(port);
	sockaddr.sin_addr.s_addr = inet_addr(addr);
	if (connect(
			socket_fd,
			(struct sockaddr *)&sockaddr,
			sizeof(sockaddr)) != 0)
	{
		close(socket_fd);
		return (-1);
	}
	return (socket_fd);
}

static void		set_timeout(int socket_fd)
{
	struct timeval tv;

	ft_bzero(&tv, sizeof(tv));
	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;
	setsockopt(socket_fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
}

int				socket_listener(const char *addr, int port)
{
	int					socket_fd;
	struct protoent		*protoent;

	protoent = getprotobyname("tcp");
	if (protoent == NULL)
	{
		return (-1);
	}
	socket_fd = socket(AF_INET, SOCK_STREAM, protoent->p_proto);
	if (socket_fd != -1)
	{
		set_timeout(socket_fd);
		socket_fd = connect_to(socket_fd, addr, port);
	}
	return (socket_fd);
}
