/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:37:12 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:37:13 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "common.h"
#include "libft.h"

static ssize_t	to_window(char *buffer, int to_fd)
{
	char		*conv;
	ssize_t		res;

	res = 0;
	conv = unix_to_window(buffer);
	if (conv != NULL)
	{
		res = write(to_fd, conv, ft_strlen(conv));
		free(conv);
	}
	return (res);
}

int				stream_data(int to_fd, int from_fd, int data_mode)
{
	char		buffer[4096];
	ssize_t		ret;
	ssize_t		writed_data;

	writed_data = 0;
	if (to_fd != -1 && from_fd != -1)
	{
		while ((ret = read(from_fd, buffer, 4095)) > 0)
		{
			if (data_mode == MODE_BINARY)
				writed_data += write(to_fd, buffer, ret);
			else
			{
				buffer[ret] = 0;
				writed_data += to_window(buffer, to_fd);
			}
		}
		return (writed_data);
	}
	return (-1);
}
