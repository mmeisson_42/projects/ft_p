/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unix_to_window.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:53:49 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:53:51 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static size_t	count_unix_newline(const char *str)
{
	size_t	newline_count;
	size_t	i;

	i = 0;
	newline_count = 0;
	while (str[i] != 0)
	{
		if (str[i] == '\n' && (i == 0 || str[i - 1] != '\r'))
		{
			newline_count++;
		}
		i++;
	}
	return (newline_count);
}

char			*unix_to_window(char *str)
{
	size_t	newline_count;
	size_t	i;
	size_t	j;
	char	*windowsed;

	i = 0;
	j = 0;
	newline_count = count_unix_newline(str);
	windowsed = malloc(sizeof(char) * (ft_strlen(str) + newline_count + 1));
	if (windowsed != NULL)
	{
		while (str[i] != 0)
		{
			if (str[i] == '\n' && (i == 0 || str[i - 1] != '\r'))
			{
				windowsed[j++] = '\r';
			}
			windowsed[j++] = str[i++];
		}
		windowsed[j] = 0;
	}
	return (windowsed);
}
