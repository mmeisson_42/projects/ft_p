/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window_to_unix.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:55:23 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:13:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

static size_t	count_windows_newline(const char *str)
{
	size_t	newline_count;
	size_t	i;

	i = 0;
	newline_count = 0;
	while (str[i] != 0)
	{
		if (str[i] == '\r' && str[i + 1] == '\n')
		{
			newline_count++;
		}
		i++;
	}
	return (newline_count);
}

char			*window_to_unix(char *str)
{
	size_t	newline_count;
	size_t	i;
	size_t	j;
	char	*unixed;

	i = 0;
	j = 0;
	newline_count = count_windows_newline(str);
	unixed = malloc(sizeof(char) * ((ft_strlen(str) - newline_count) + 1));
	if (unixed != NULL)
	{
		while (str[i] != 0)
		{
			if (str[i] == '\r' && str[i + 1] == '\n')
			{
				i++;
			}
			unixed[j++] = str[i++];
		}
		unixed[j] = 0;
	}
	return (unixed);
}
