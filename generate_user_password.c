/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   register_user.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:15:51 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:15:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int		main(int argc, char **argv)
{
	int		i;
	int		hash;
	int		c;
	char	*password;

	i = 1;
	while (i < argc)
	{
		hash = 1337;
		password = argv[i];
		while ((c = *password))
		{
			hash = ((hash << 5) + hash) + c;
			password++;
		}
		ft_printf("%s -> %x\n", argv[i], hash);
		i++;
	}
}
