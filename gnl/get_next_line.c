/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 11:39:13 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 18:33:02 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "get_next_line.h"
#include "libft.h"

static struct s_list	*remove_fd(struct s_list *files, int fd)
{
	struct s_file	*f;
	struct s_list	*next;

	if (files != NULL)
	{
		f = files->content;
		next = files->next;
		if (f->fd == fd)
		{
			free(f->buffer);
			free(f);
			free(files);
			return (next);
		}
		files->next = remove_fd(files->next, fd);
		return (files);
	}
	return (NULL);
}

static struct s_file	*get_file(int fd, t_list **files)
{
	struct s_file	new;

	if (*files == NULL)
	{
		new.fd = fd;
		new.buffer = ft_strdup("");
		if (new.buffer == NULL)
			return (NULL);
		*files = ft_lstnew(&new, sizeof(new));
		if (*files == NULL)
		{
			free(new.buffer);
			return (NULL);
		}
		return ((*files)->content);
	}
	if (((struct s_file *)(*files)->content)->fd == fd)
		return ((*files)->content);
	return (get_file(fd, &(*files)->next));
}

static int				get_line(struct s_file *file, char **line)
{
	ssize_t		ret;
	char		*tmp;
	char		buffer[BUFF_SIZE + 1];

	if (file->buffer && (tmp = ft_strchr(file->buffer, '\n')))
	{
		*tmp = 0;
		*line = ft_strdup(file->buffer);
		ft_strcpy(file->buffer, tmp + 1);
		return (1);
	}
	while ((ret = read(file->fd, buffer, BUFF_SIZE)) > 0)
	{
		buffer[ret] = 0;
		file->buffer = ft_strover(file->buffer, buffer);
		if (file->buffer && (tmp = ft_strchr(file->buffer, '\n')))
		{
			*tmp = 0;
			*line = ft_strdup(file->buffer);
			ft_strcpy(file->buffer, tmp + 1);
			return (1);
		}
	}
	return ((int)ret);
}

int						get_next_line(int fd, char **line)
{
	static struct s_list	*files = NULL;
	struct s_file			*current;
	int						ret;

	current = get_file(fd, &files);
	if (current == NULL)
	{
		return (-1);
	}
	ret = get_line(current, line);
	if (ret != 1)
	{
		files = remove_fd(files, fd);
	}
	return (ret);
}
