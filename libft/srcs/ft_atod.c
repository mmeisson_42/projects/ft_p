/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atod.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 16:51:37 by mmeisson          #+#    #+#             */
/*   Updated: 2017/09/27 16:57:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double		ft_atod(const char *str)
{
	double		nb;
	double		floating;
	int			neg;

	while (ft_isspace(*str))
		str++;
	nb = 0;
	floating = 0;
	neg = (*str == '-') ? 1 : 0;
	str += (*str == '+' || *str == '-') ? 1 : 0;
	while (*str && ft_isdigit(*str))
	{
		nb = nb * 10 + (*str - 48);
		str++;
	}
	if (*str == '.')
	{
		while (*str && ft_isdigit(*str))
		{
			floating = (floating + (*str - 48)) / 10;
			str++;
		}
	}
	return (neg) ? (-(nb + floating)) : (nb + floating);
}
