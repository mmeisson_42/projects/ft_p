/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnendl_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/30 14:52:37 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/30 15:08:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

void		ft_putnendl_fd(const char *str, int fd, size_t n)
{
	write(fd, str, ft_strnlen(str, n));
	write(fd, "\n", n);
}
