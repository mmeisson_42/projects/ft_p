/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:03:06 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/29 12:23:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *d, const char *s, size_t size)
{
	size_t		i;
	size_t		j;
	size_t		f_size;

	i = 0;
	j = 0;
	f_size = ft_strlen(d) + ft_strlen(s);
	while (d[i] != 0)
	{
		if (i == size)
			return (size + ft_strlen(s));
		i++;
	}
	while (s[j] && i + j + 1 < size)
	{
		d[i + j] = s[j];
		j++;
	}
	d[i + j] = '\0';
	return (f_size);
}
