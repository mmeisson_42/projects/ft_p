/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 11:56:54 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/31 19:00:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s, size_t n)
{
	char *dest;

	dest = ft_memalloc(sizeof(char) * (n + 1));
	if (dest != NULL)
	{
		ft_strncpy(dest, s, n);
		dest[n] = 0;
	}
	return (dest);
}
