/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnover.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 20:56:24 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/27 03:05:10 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnover(char *erase, const char *str, size_t n)
{
	char	*new_str;
	int		len_erase;
	int		len_str;

	len_erase = (erase) ? ft_strlen(erase) : 0;
	len_str = (str) ? ft_strlen(str) : 0;
	new_str = ft_memalloc(sizeof(char) * (len_erase + len_str + 1));
	if (new_str)
	{
		if (erase)
			ft_memcpy(new_str, erase, len_erase);
		if (str)
			ft_strncpy(new_str + len_erase, str, n);
		if (erase)
			free(erase);
	}
	return (new_str);
}
