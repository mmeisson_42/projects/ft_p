/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrealloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 23:13:15 by mmeisson          #+#    #+#             */
/*   Updated: 2016/05/03 03:32:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrealloc(char *str, size_t size)
{
	char		*ret;

	if (!(ret = ft_memalloc(size)))
		return (NULL);
	if (str)
	{
		ft_strncpy(ret, str, size);
		free(str);
	}
	return (ret);
}
