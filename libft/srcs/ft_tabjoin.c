/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 16:05:35 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/17 09:21:54 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static void		fill_join(char *res, const char **tab, char separator)
{
	size_t		i;
	size_t		len;

	i = 0;
	len = 0;
	while (tab[i] != NULL)
	{
		if (tab[i][0] != '\0')
		{
			ft_strcpy(res + len, tab[i]);
			len += ft_strlen(tab[i]) + !!separator;
			res[len - 1] = separator;
		}
		i++;
	}
	if (len == 0)
		res[0] = 0;
	else
		res[len - 1] = 0;
}

char			*ft_tabjoin(const char **tab, char separator)
{
	size_t		i;
	size_t		len;
	char		*res;

	i = 0;
	len = 0;
	while (tab[i] != NULL)
	{
		len += ft_strlen(tab[i++]) + !!separator;
	}
	res = malloc(sizeof(char) * (len + 1));
	if (res != NULL)
	{
		fill_join(res, tab, separator);
	}
	return (res);
}
