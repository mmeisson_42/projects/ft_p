/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_merge_list.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/03 11:21:40 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/15 11:09:24 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		manage_insertion(t_list **node, t_list **head, t_list **tail)
{
	t_list	*prev;

	prev = *node;
	*node = prev->next;
	prev->next = NULL;
	if (*tail)
	{
		(*tail)->next = prev;
		*tail = (*tail)->next;
	}
	else
	{
		*head = prev;
		*tail = prev;
	}
}

static t_list	*insertion_merge(t_list *slow, t_list *fast, int (*cmp)())
{
	t_list	*head;
	t_list	*tail;

	head = NULL;
	tail = NULL;
	while (slow && fast)
	{
		if (cmp(slow->content, fast->content) < 0)
			manage_insertion(&slow, &head, &tail);
		else
			manage_insertion(&fast, &head, &tail);
	}
	while (slow)
		manage_insertion(&slow, &head, &tail);
	while (fast)
		manage_insertion(&fast, &head, &tail);
	return (head);
}

t_list			*merge_list(t_list *list, int (*cmp)())
{
	t_list	*slow;
	t_list	*fast;
	t_list	*prev;

	if (list && list->next)
	{
		slow = list;
		fast = list->next;
		prev = slow;
		while (fast && fast->next)
		{
			prev = slow;
			slow = slow->next;
			fast = fast->next->next;
		}
		prev = slow;
		slow = slow->next;
		prev->next = NULL;
	}
	else
		return (list);
	fast = merge_list(slow, cmp);
	slow = merge_list(list, cmp);
	return (insertion_merge(slow, fast, cmp));
}
