/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:15:58 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:15:59 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# define ROOT_DIRECTORY "/Users/mmeisson/ft_p"
# define SERVER_MAX_CONNECTION	42
# define DEFAULT_USER "anonymous"
# define DB_PATH_USER "/Users/mmeisson/ft_p/.db_user"

# define ACTIV_IS_DEFAULT	1
# define STREAM_NW_RD		1
# define STREAM_NW_WR		0

union			u_port
{
	unsigned short	sh;
	unsigned char	ch[2];
};

struct			s_context
{
	char	*user;
	char	*password;

	char	*pwd;
	char	*root_directory;
	int		logged;

	int		mode_activ;
	int		port_out;
	int		fd_out;
	int		server_fd;
	int		data_mode;

	int		in_transfer;
	int		has_to_die;
};

typedef void	(*t_command_callback)(const char **, struct s_context *);
struct			s_command_panel
{
	char				name[6];
	t_command_callback	callback;
};

typedef int		(*t_middleware)(void ** command, struct s_context *ctx);

void			split_server(void **command, struct s_context *ctx);
char			send_response(int status, const char *body);
void			server_entry(int client_socket, struct s_context *ctx);

char			*simplify_path(const char *path);
char			*path_join(const char *first, const char *second);
void			stream_file(
		const char *file_name,
		struct s_context *ctx,
		int stream_mode);

/*
**	Middlewares
*/
int				split_command(void **command, struct s_context *ctx);
int				check_rights(void **command, struct s_context *ctx);
int				check_path(void **command, struct s_context *ctx);

void			call_command(
	void *command,
	struct s_context *ctx,
	t_command_callback callback);
/*
**	Commands
*/
void			list(const char **command, struct s_context *ctx);
void			cwd(const char **command, struct s_context *ctx);
void			user(const char **command, struct s_context *ctx);
void			pass(const char **command, struct s_context *ctx);
void			pasv(const char **command, struct s_context *ctx);
void			pwd(const char **command, struct s_context *ctx);
void			quit(const char **command, struct s_context *ctx);
void			port(const char **command, struct s_context *ctx);
void			retr(const char **command, struct s_context *ctx);
void			stor(const char **command, struct s_context *ctx);
void			cdup(const char **command, struct s_context *ctx);
void			syst(const char **command, struct s_context *ctx);
void			noop(const char **command, struct s_context *ctx);
void			dele(const char **command, struct s_context *ctx);
void			rmd(const char **command, struct s_context *ctx);
void			mkd(const char **command, struct s_context *ctx);
void			type(const char **command, struct s_context *ctx);

#endif
