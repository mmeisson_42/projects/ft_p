/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   call_command.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:12:36 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:12:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "server.h"
#include "libft.h"

static const t_middleware	g_middlewares[] = {
	split_command,
	check_rights,
	check_path,
};

void						call_command(
	void *command,
	struct s_context *ctx,
	t_command_callback callback)
{
	size_t	i;
	int		res;

	i = 0;
	while (i < (sizeof(g_middlewares) / sizeof(g_middlewares[0])))
	{
		if ((res = g_middlewares[i](&command, ctx)) != 0)
		{
			ft_tabdel((void *)&command);
			return ;
		}
		i++;
	}
	callback(command, ctx);
	ft_tabdel((void *)&command);
}
