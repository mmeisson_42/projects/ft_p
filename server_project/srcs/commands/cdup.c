/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cdup.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:11:08 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:20:54 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "server.h"
#include "libft.h"

static char		*get_parent_path(struct s_context *ctx)
{
	char	*tmp_path;
	char	*required_path;

	tmp_path = ft_strjoin(ctx->pwd, "/..");
	if (tmp_path == NULL)
	{
		ft_putstr("552 Server busy\n");
		return (NULL);
	}
	required_path = simplify_path(tmp_path);
	free(tmp_path);
	if (required_path == NULL)
	{
		ft_putstr("552 Server busy\n");
		return (NULL);
	}
	return (required_path);
}

void			cdup(const char **command, struct s_context *ctx)
{
	char	*required_path;

	(void)command;
	required_path = get_parent_path(ctx);
	if (required_path == NULL)
		return ;
	if (ft_strncmp(
			ctx->root_directory,
			required_path,
			ft_strlen(ctx->root_directory)) != 0
		|| chdir(required_path) == -1)
	{
		ft_putstr("550 Directory not accessible\n");
		return ;
	}
	free(required_path);
	ctx->pwd = getcwd(NULL, 0);
	if (ctx->pwd == NULL)
	{
		ft_putstr("552 Server busy\n");
		return ;
	}
	ft_putendl("200 OK");
}
