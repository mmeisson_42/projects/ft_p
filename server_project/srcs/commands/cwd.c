/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:12:21 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:12:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "server.h"
#include "libft.h"

void	cwd(const char **command, struct s_context *ctx)
{
	if (ft_tablen(command) != 2)
	{
		ft_putstr("501 Syntax error in parameters\n");
		return ;
	}
	if (chdir(command[1]) == -1)
	{
		ft_putstr("550 Directory not accessible\n");
		return ;
	}
	ctx->pwd = getcwd(NULL, 0);
	if (ctx->pwd == NULL)
	{
		ft_putstr("552 Server busy\n");
		return ;
	}
	ft_putendl("200 OK");
}
