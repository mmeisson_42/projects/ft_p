/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:09:39 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:19:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <stdlib.h>
#include "server.h"
#include "libft.h"
#include "common.h"
#include "ft_stdio.h"
#include <sys/socket.h>

static char		**fill_new_command(char **filtered, const char **command)
{
	size_t		command_index;
	size_t		filtered_index;

	command_index = 1;
	filtered_index = 2;
	while (command[command_index] != NULL)
	{
		filtered[filtered_index] = ft_strdup(command[command_index++]);
		if (filtered[filtered_index++] == NULL)
		{
			ft_tabdel(&filtered);
			return (NULL);
		}
	}
	filtered[filtered_index] = NULL;
	return (filtered);
}

static char		**filter_options(const char **command)
{
	char		**filtered;

	filtered = malloc(sizeof(char *) * (ft_tablen(command) + 2));
	if (filtered == NULL)
	{
		return (NULL);
	}
	if ((filtered[0] = ft_strdup(command[0])) == NULL
		|| (filtered[1] = ft_strdup("-l")) == NULL)
	{
		ft_tabdel(&filtered);
	}
	return (fill_new_command(filtered, command));
}

static void		send_list(int fd_list, struct s_context *ctx)
{
	int		fd_out;

	if (ctx->fd_out != -1 && ctx->port_out != -1)
	{
		if (ctx->mode_activ == 1)
			fd_out = ctx->fd_out;
		else
			fd_out = accept(ctx->fd_out, NULL, 0);
		ft_putstr("125 Data connection already open; transfer starting\n");
		if (stream_data(fd_out, fd_list, ctx->data_mode) == -1)
			ft_putstr("550 Requested action not taken. File unavailable\n");
		else
			ft_putstr("226 Closing data connection\n");
		if (ctx->mode_activ == 1)
			close(ctx->fd_out);
		close(fd_out);
		ctx->fd_out = -1;
		ctx->port_out = -1;
		ctx->mode_activ = 1;
	}
	else
		ft_putstr("425 Can't open data connection\n");
}

void			list(const char **command, struct s_context *ctx)
{
	pid_t	pid;
	int		pipes[2];
	char	**filtered_options;

	filtered_options = filter_options(command);
	if (filtered_options == NULL)
	{
		ft_putstr("451 Requested action aborted. Local error in processing\n");
		return ;
	}
	pipe(pipes);
	if ((pid = fork()) == 0)
	{
		close(pipes[0]);
		dup2(pipes[1], 1);
		execve("/bin/ls", filtered_options, NULL);
	}
	else
	{
		close(pipes[1]);
		send_list(pipes[0], ctx);
		close(pipes[0]);
	}
	ft_tabdel(&filtered_options);
}
