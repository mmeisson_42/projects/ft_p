/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rmd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 19:16:47 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 19:16:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <sys/types.h>
#include "server.h"
#include "libft.h"

void	mkd(const char **command, struct s_context *ctx)
{
	int		res;

	(void)ctx;
	if (ft_tablen(command) != 2)
	{
		ft_putstr("501 Syntax error in parameters\n");
		return ;
	}
	res = mkdir(command[1], S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
	if (res == 0)
	{
		ft_putstr("252 Directory created\n");
	}
	else
	{
		ft_putstr("421 Could not create this directory\n");
	}
}
