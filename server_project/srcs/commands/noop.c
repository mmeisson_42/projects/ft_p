/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   noop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 19:04:55 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 19:05:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "server.h"

void		noop(const char **command, struct s_context *ctx)
{
	(void)command;
	(void)ctx;
	ft_putstr("200\n");
}
