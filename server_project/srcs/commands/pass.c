/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pass.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:10:39 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:20:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"
#include "libft.h"

static char		*hash_password(const char *password)
{
	int hash;
	int c;

	hash = 1337;
	while ((c = *password))
	{
		hash = ((hash << 5) + hash) + c;
		password++;
	}
	return (ft_itoa_base(hash, 16));
}

static void		validate_password(const char *hash, struct s_context *ctx)
{
	if (ctx->password != NULL && ft_strcmp(ctx->password, hash) == 0)
	{
		ctx->logged = 1;
		ft_putstr("230 User logged in\n");
	}
	else
	{
		ft_strdel(&ctx->user);
		ft_putstr("430 Invalid username or password\n");
	}
	ft_strdel(&ctx->password);
}

void			pass(const char **command, struct s_context *ctx)
{
	char	*hash;

	if (ft_strcmp(ctx->user, DEFAULT_USER) == 0)
	{
		ft_putendl("230 User logged in, proceed. Logged out if appropriate. ");
		return ;
	}
	if (ft_tablen(command) != 2)
	{
		ft_putstr("501 Syntax error in parameters\n");
		return ;
	}
	ctx->logged = 0;
	hash = hash_password(command[1]);
	if (hash == NULL)
	{
		ft_putstr("501 Server busy");
	}
	else
	{
		validate_password(hash, ctx);
	}
	free(hash);
}
