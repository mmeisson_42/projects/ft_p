/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pasv.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:12:04 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:12:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "server.h"
#include "libft.h"
#include "ft_stdio.h"
#include "common.h"

void	pasv(const char **command, struct s_context *ctx)
{
	union u_port	port;
	int				fd;

	(void)command;
	ctx->mode_activ = 0;
	port.sh = 1024;
	while (port.sh < USHRT_MAX)
	{
		fd = socket_binded(port.sh);
		if (fd != -1)
		{
			ctx->port_out = port.sh;
			ctx->fd_out = fd;
			ft_printf(
				"227 Entering passiv mode (127,0,0,1,%hhd,%hhd)\n",
				port.ch[1],
				port.ch[0]);
			return ;
		}
		port.sh++;
	}
	ft_putstr("550 Could not create connection\n");
}
