/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   port.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:08:06 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:09:46 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "server.h"
#include "common.h"
#include "libft.h"

static int		connect_(char **network, struct s_context *ctx)
{
	int port;
	int	fd;

	port = ft_atoi(network[1]);
	fd = socket_listener(network[0], port);
	if (fd != -1)
	{
		ctx->mode_activ = 1;
		ctx->fd_out = fd;
		ctx->port_out = port;
		ft_putstr("200\n");
	}
	else
	{
		ctx->fd_out = -1;
		ctx->port_out = -1;
		ft_putstr("500\n");
	}
	return (fd);
}

void			port(const char **command, struct s_context *ctx)
{
	char	**addr;

	if (ft_tablen(command) != 2)
	{
		ft_putstr("501 Syntax error in parameters\n");
		return ;
	}
	addr = get_network(command[1]);
	if (addr == NULL)
	{
		ft_putstr("550 Could not establich connection\n");
		return ;
	}
	connect_(addr, ctx);
	ft_tabdel(&addr);
}
