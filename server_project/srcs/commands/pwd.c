/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:10:17 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:10:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"
#include "ft_stdio.h"

void	pwd(const char **command, struct s_context *ctx)
{
	(void)command;
	ft_printf("257 \"%s\"\n", ctx->pwd);
}
