/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:10:00 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:10:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "server.h"
#include "libft.h"

void		quit(const char **command, struct s_context *ctx)
{
	(void)command;
	ft_putstr("221 Disconnected\n");
	if (ctx->fd_out != -1)
	{
		close(ctx->fd_out);
	}
	if (ctx->fd_out != -1)
	{
		close(ctx->server_fd);
	}
	exit(0);
}
