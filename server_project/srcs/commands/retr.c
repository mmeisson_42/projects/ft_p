/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   retr.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:18:33 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:18:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/socket.h>
#include "server.h"
#include "libft.h"
#include "common.h"
#include "ft_stdio.h"

void			retr(const char **command, struct s_context *ctx)
{
	if (ft_tablen(command) != 2)
	{
		ft_putstr("501 Syntax error in parameters\n");
		return ;
	}
	if (ctx->in_transfer == 1)
	{
		ft_putstr("410 Already transfering\n");
		return ;
	}
	stream_file(command[1], ctx, STREAM_NW_WR);
}
