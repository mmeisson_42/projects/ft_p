/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rmd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 19:16:47 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 19:16:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "server.h"
#include "libft.h"

void	rmd(const char **command, struct s_context *ctx)
{
	int		res;

	(void)ctx;
	if (ft_tablen(command) != 2)
	{
		ft_putstr("501 Syntax error in parameters\n");
		return ;
	}
	res = rmdir(command[1]);
	if (res == 0)
	{
		ft_putstr("250 Directory removed\n");
	}
	else
	{
		ft_putstr("450 Could not remove this directory\n");
	}
}
