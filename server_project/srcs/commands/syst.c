/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syst.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 18:59:50 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 18:59:51 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include "server.h"
#include "common.h"
#include "libft.h"
#include "ft_stdio.h"
#include "get_next_line.h"

static void		exec_uname(void)
{
	const char		*uname_args[] = {
		"/usr/bin/uname",
		NULL,
	};

	execve(uname_args[0], (char *const *)uname_args, NULL);
}

static void		monitor_uname(pid_t pid, int uname_fd)
{
	int		ret;
	char	*response;

	wait4(pid, &ret, 0, NULL);
	if (WEXITSTATUS(ret) == 0)
	{
		if (get_next_line(uname_fd, &response) > 0)
		{
			ft_printf("215 %s\n", response);
			free(response);
			return ;
		}
	}
	ft_putstr("451 Failed to execute");
}

void			syst(const char **command, struct s_context *ctx)
{
	int		pipes[2];
	int		pid;

	(void)command;
	(void)ctx;
	pipe(pipes);
	pid = fork();
	if (pid == 0)
	{
		close(pipes[0]);
		dup2(pipes[1], 1);
		exec_uname();
	}
	else
	{
		close(pipes[1]);
		monitor_uname(pid, pipes[0]);
		close(pipes[0]);
	}
}
