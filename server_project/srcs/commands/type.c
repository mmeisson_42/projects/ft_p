/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/19 14:22:29 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/19 14:23:15 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "server.h"
#include "common.h"
#include "ft_stdio.h"

void		type(const char **commands, struct s_context *ctx)
{
	const char		ascii_modes[] = "A";
	const char		binary_modes[] = "IBRV";

	if (ft_tablen(commands) == 1)
	{
		ft_printf(
				"200 Actual type: %s\n",
				(ctx->data_mode == MODE_ASCII) ? "ascii" : "binary");
	}
	else if (ft_strchr(ascii_modes, commands[1][0]) != NULL)
	{
		ctx->data_mode = MODE_ASCII;
		ft_putstr("200 OK\n");
	}
	else if (ft_strchr(binary_modes, commands[1][0]) != NULL)
	{
		ctx->data_mode = MODE_BINARY;
		ft_putstr("200 OK\n");
	}
	else
	{
		ft_putstr("501 Syntax error in parameters\n");
	}
}
