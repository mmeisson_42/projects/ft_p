/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 17:22:28 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:22:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "server.h"
#include "libft.h"
#include "get_next_line.h"
#include "ft_stdio.h"

static void		rand_sleep(void)
{
	struct timespec		t;

	srand(time(NULL));
	t.tv_sec = 1;
	t.tv_nsec = rand() >> 2;
	nanosleep(&t, NULL);
}

static int		prepare_session(
		struct s_context *ctx,
		char *user,
		char *password)
{
	ctx->user = user;
	ft_strdel(&ctx->password);
	ft_strdel(&ctx->root_directory);
	ft_strdel(&ctx->pwd);
	ctx->password = ft_strdup(password);
	ctx->root_directory = path_join(ROOT_DIRECTORY, user);
	ctx->pwd = path_join(ROOT_DIRECTORY, user);
	if (chdir(ctx->root_directory) == -1)
	{
		ft_putendl_fd(ctx->root_directory, 2);
		if (mkdir(
				ctx->root_directory,
				S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) == -1)
		{
			ft_putendl("550 Requested action not taken. File unavailable");
			return (1);
		}
		if (chdir(ctx->root_directory) == -1)
		{
			ft_putendl("550 Requested action not taken. File unavailable");
			return (1);
		}
	}
	return (0);
}

static int		load_user(const char *user, struct s_context *ctx)
{
	int		fd;
	char	*db_user;
	char	*end_user;

	if ((fd = open(DB_PATH_USER, O_RDONLY)) == -1)
	{
		ft_dprintf(2, "[WARNING] User database not found\n");
		return (fd);
	}
	while (get_next_line(fd, &db_user) > 0)
	{
		if ((end_user = ft_strchr(db_user, ' ')))
		{
			*end_user = 0;
			if (ft_strcmp(user, db_user) == 0)
			{
				prepare_session(ctx, db_user, end_user + 1);
				close(fd);
				return (0);
			}
		}
		ft_strdel(&db_user);
	}
	close(fd);
	return (1);
}

void			user(const char **command, struct s_context *ctx)
{
	if (ft_tablen(command) > 2)
	{
		ft_putendl("501 Syntax error in parameters or arguments");
		return ;
	}
	rand_sleep();
	if (command[1] == NULL || ft_strcmp(command[1], DEFAULT_USER) == 0)
	{
		ctx->logged = true;
		if (prepare_session(ctx, DEFAULT_USER, "") == 0)
		{
			ft_putstr("230 User logged in\n");
		}
	}
	else
	{
		if (load_user(command[1], ctx) != 0)
		{
			ft_putstr("430 Invalid username or password\n");
			return ;
		}
		ft_putstr("331 User name okay, need password\n");
	}
	return ;
}
