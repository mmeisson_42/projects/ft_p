/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:56:11 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:15:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "server.h"

static const struct s_list		g_command_to_check[] = {
	{ .content = "LIST", .content_size = 5,
		.next = (struct s_list *)&g_command_to_check[1] },
	{ .content = "CWD", .content_size = 4,
		.next = (struct s_list *)&g_command_to_check[2] },
	{ .content = "RETR", .content_size = 5,
		.next = (struct s_list *)&g_command_to_check[3] },
	{ .content = "STOR", .content_size = 5,
		.next = NULL },
};

static char		**filter_out_options(char **command_str)
{
	int		i;

	i = 1;
	while (command_str[i]
		&& command_str[i][0] == '-'
		&& ft_strcmp(command_str[i], "--"))
	{
		i++;
	}
	return (command_str + i);
}

static void		*get_simplified_path(
	const char *user_path,
	struct s_context *ctx)
{
	char	*path;
	char	*simplified_path;

	if (user_path[0] == '/')
		path = ft_strdup(user_path);
	else
		path = path_join(ctx->pwd, user_path);
	if (path == NULL)
	{
		ft_putstr("550 Server busy\n");
		return (NULL);
	}
	simplified_path = simplify_path(path);
	ft_strdel(&path);
	return (simplified_path);
}

static int		validate_path(const char *user_path, struct s_context *ctx)
{
	char		*simplified_path;

	simplified_path = get_simplified_path(user_path, ctx);
	if (simplified_path == NULL)
		return (1);
	if (ft_strncmp(
			ctx->root_directory,
			simplified_path,
			ft_strlen(ctx->root_directory) - 1))
	{
		free(simplified_path);
		ft_putstr("550 Cannot access this path\n");
		return (1);
	}
	free(simplified_path);
	return (0);
}

int				check_path(void **command, struct s_context *ctx)
{
	char		**command_str;
	size_t		i;

	command_str = (char **)*command;
	if (ft_lstin(
			command_str[0],
			ft_strlen(command_str[0]) + 1,
			g_command_to_check))
	{
		i = 0;
		command_str = filter_out_options(command_str);
		while (command_str[i])
		{
			if (validate_path(command_str[i], ctx) != 0)
				return (1);
			i++;
		}
	}
	return (0);
}
