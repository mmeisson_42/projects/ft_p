/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_rights.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:56:20 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:56:21 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "server.h"
#include "libft.h"

int		check_rights(void **command, struct s_context *ctx)
{
	const char	*command_name = ((char **)*command)[0];

	if (ft_strcmp(command_name, "USER") == 0
		|| ft_strcmp(command_name, "QUIT") == 0)
	{
		return (0);
	}
	else if (ft_strcmp(command_name, "PASS") == 0)
	{
		if (ctx->user == NULL)
		{
			ft_putstr("430 Incorrect identifier\n");
			return (1);
		}
		return (0);
	}
	else if (ctx->logged == 0)
	{
		ft_putstr("332 Need account\n");
		return (1);
	}
	return (0);
}
