/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_command.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:56:24 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:02:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "server.h"
#include "libft.h"

int		split_command(void **command, struct s_context *ctx)
{
	(void)ctx;
	*command = ft_strsplit(*command, ' ');
	if (*command == NULL)
	{
		ft_putstr("550 Server busy\n");
		return (1);
	}
	return (0);
}
