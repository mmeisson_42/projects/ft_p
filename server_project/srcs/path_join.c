/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_join.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:55:55 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:15:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

char	*path_join(const char *first, const char *second)
{
	char	*str;
	int		has_no_slash;
	size_t	first_len;
	size_t	second_len;

	first_len = ft_strlen(first);
	second_len = ft_strlen(second);
	has_no_slash = (first[first_len - 1] != '/');
	str = ft_memalloc(first_len + second_len + 1 + has_no_slash);
	if (str != NULL)
	{
		ft_memcpy(str, first, first_len);
		if (has_no_slash)
		{
			str[first_len] = '/';
		}
		ft_memcpy(str + first_len + has_no_slash, second, second_len + 1);
	}
	return (str);
}
