/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:54:33 by mmeisson          #+#    #+#             */
/*   Updated: 2019/01/17 09:46:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>
#include "server.h"
#include "common.h"
#include "libft.h"
#include "ft_stdio.h"

static void		sig_handler(void *param)
{
	static struct s_context		*c = NULL;

	if (c == NULL)
	{
		c = param;
		return ;
	}
	ft_dprintf(
		2,
		"Signal received (%lu)\n\tClosing connections...\n",
		(size_t)param);
	if (c->fd_out != -1)
	{
		close(c->fd_out);
	}
	exit(1);
}

static void		sig(void)
{
	int		i;

	i = 0;
	while (i < SIGPIPE)
	{
		signal(i, (void (*)(int))sig_handler);
		i++;
	}
}

void			set_context(const char *string_port, struct s_context *ctx)
{
	int		port;

	ft_bzero(ctx, sizeof(*ctx));
	sig_handler(ctx);
	sig();
	ctx->fd_out = -1;
	ctx->server_fd = -1;
	ctx->mode_activ = ACTIV_IS_DEFAULT;
	ctx->port_out = -1;
	ctx->data_mode = MODE_ASCII;
	if (chdir(ROOT_DIRECTORY) == -1)
	{
		ft_dprintf(2, "FATAL_ERROR: " ROOT_DIRECTORY " does not exists\n");
		exit(EXIT_FAILURE);
	}
	port = ft_atoi(string_port);
	ctx->server_fd = socket_binded(port);
	if (ctx->server_fd == -1)
	{
		ft_dprintf(2, "Could not bind socket to port %d :(\n", port);
		exit(EXIT_FAILURE);
	}
}

int				main(int argc, const char **argv)
{
	int					connection_entry;
	struct s_context	ctx;

	if (argc != 2 || ft_atoi(argv[1]) == 0)
	{
		ft_dprintf(2, "Usage: %s port\n", argv[0]);
		return (EXIT_FAILURE);
	}
	set_context(argv[1], &ctx);
	while (42)
	{
		connection_entry = accept(ctx.server_fd, NULL, 0);
		if (fork() == 0)
		{
			close(ctx.server_fd);
			server_entry(connection_entry, &ctx);
			close(connection_entry);
			return (EXIT_SUCCESS);
		}
		else
		{
			close(connection_entry);
		}
	}
	return (EXIT_SUCCESS);
}
