/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_entry.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:55:18 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:14:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "server.h"
#include "libft.h"
#include "common.h"
#include "ft_stdio.h"
#include "get_next_line.h"

static const struct s_command_panel		g_command_panel[] = {
	{.name = LIST_COMMAND, .callback = list},
	{.name = CWD_COMMAND, .callback = cwd},
	{.name = USER_COMMAND, .callback = user},
	{.name = PASS_COMMAND, .callback = pass},
	{.name = PASV_COMMAND, .callback = pasv},
	{.name = PWD_COMMAND, .callback = pwd},
	{.name = QUIT_COMMAND, .callback = quit},
	{.name = PORT_COMMAND, .callback = port},
	{.name = RETR_COMMAND, .callback = retr},
	{.name = STOR_COMMAND, .callback = stor},
	{.name = CDUP_COMMAND, .callback = cdup},
	{.name = SYST_COMMAND, .callback = syst},
	{.name = NOOP_COMMAND, .callback = noop},
	{.name = RMD_COMMAND, .callback = rmd},
	{.name = MKD_COMMAND, .callback = mkd},
	{.name = DELE_COMMAND, .callback = dele},
	{.name = TYPE_COMMAND, .callback = type},
};

static void		process_command(char *command, struct s_context *ctx)
{
	size_t			i;

	i = 0;
	while (i < (sizeof(g_command_panel) / sizeof(g_command_panel[0])))
	{
		if (ft_strncmp(
					command, g_command_panel[i].name,
					ft_strlen(g_command_panel[i].name)) == 0)
		{
			call_command(command, ctx, g_command_panel[i].callback);
			return ;
		}
		i++;
	}
	ft_putendl("502 Not implemented");
	return ;
}

void			server_entry(int client_socket, struct s_context *ctx)
{
	char				*line;

	dup2(client_socket, 1);
	dup2(client_socket, 0);
	line = NULL;
	ctx->pwd = getcwd(NULL, 0);
	if (ctx->pwd == NULL)
	{
		ft_putstr("500 server busy\n");
		return ;
	}
	ft_putstr("220 Connected\n");
	while (get_next_line(0, &line) > 0)
	{
		ft_strreplace(line, 13, 0);
		ft_dprintf(2, "received '%s' %hhd\n", line, line[ft_strlen(line) - 1]);
		process_command(line, ctx);
		ft_strdel(&line);
	}
}
