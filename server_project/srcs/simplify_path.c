/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simplify_path.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:55:45 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 17:13:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

void	remove_component(char **components, size_t index)
{
	free(components[index]);
	while (components[index])
	{
		components[index] = components[index + 1];
		index++;
	}
}

void	filter_components(char **components)
{
	size_t		i;

	i = 0;
	while (components[i] != NULL)
	{
		if (ft_strcmp(components[i], "..") == 0)
		{
			remove_component(components, i);
			if (i > 0)
			{
				i--;
				remove_component(components, i);
			}
			continue ;
		}
		else if (ft_strcmp(components[i], ".") == 0)
		{
			remove_component(components, i);
		}
		i++;
	}
}

char	*simplify_path(const char *str)
{
	char	**components;
	char	*simplified_path;
	char	*tmp;

	components = ft_strsplit(str, '/');
	if (components == NULL)
	{
		return (NULL);
	}
	filter_components(components);
	tmp = ft_tabjoin((const char **)components, '/');
	if (tmp == NULL)
		return (NULL);
	ft_tabdel(&components);
	simplified_path = malloc(ft_strlen(tmp) + 2);
	if (simplified_path == NULL)
	{
		free(tmp);
		return (NULL);
	}
	simplified_path[0] = '/';
	ft_strcpy(simplified_path + 1, tmp);
	free(tmp);
	return (simplified_path);
}
