/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 16:53:54 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/16 16:53:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include "libft.h"
#include "server.h"
#include "common.h"

static void		exec_stream(
		const char *file_name,
		struct s_context *ctx,
		int stream_mode)
{
	int		fd_file;
	int		stream_fd;

	if (stream_mode == STREAM_NW_RD)
		fd_file = open(file_name, O_WRONLY | O_CREAT, 0644);
	else
		fd_file = open(file_name, O_RDONLY);
	if (fd_file == -1)
		exit(EXIT_FAILURE);
	if (ctx->mode_activ == 1)
		stream_fd = ctx->fd_out;
	else
		stream_fd = accept(ctx->fd_out, NULL, 0);
	if (stream_mode == STREAM_NW_RD)
		stream_data(fd_file, stream_fd, ctx->data_mode);
	else
		stream_data(stream_fd, fd_file, ctx->data_mode);
	close(stream_fd);
	if (ctx->mode_activ == 0)
		close(ctx->fd_out);
	close(fd_file);
	exit(EXIT_SUCCESS);
}

static void		monitor_stream(pid_t child_pid, struct s_context *ctx)
{
	int		ret;

	ft_putstr("125 Data connection already open; transfer starting\n");
	wait4(child_pid, &ret, 0, NULL);
	close(ctx->fd_out);
	if (WEXITSTATUS(ret) == EXIT_SUCCESS)
	{
		ft_putstr("226 Closing data connection\n");
	}
	else
	{
		ft_putstr("550 Requested action not taken. File unavailable\n");
	}
	ctx->fd_out = -1;
	ctx->port_out = -1;
}

void			stream_file(
		const char *file_name,
		struct s_context *ctx,
		int stream_mode)
{
	pid_t	pid;

	if (ctx->port_out != -1 && ctx->fd_out != -1)
	{
		ctx->in_transfer = 1;
		pid = fork();
		if (pid == 0)
		{
			exec_stream(file_name, ctx, stream_mode);
		}
		else
		{
			monitor_stream(pid, ctx);
			ctx->in_transfer = 0;
		}
	}
	else
	{
		ft_putstr("426 Connection is closed. Transfer aborted\n");
	}
}
